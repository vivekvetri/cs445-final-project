#Author : Vivek Gnanavelu
#Date : 05/01/2020

import base64
import json
import os
import sys
import time
import tkinter as tk
from random import random, seed, randint
import cv2
import dlib
import face_recognition
from time import sleep
import requests


def find_who(img_str):
    url = "http://localhost:5000/recognize"
    img_str = 'data:image/jpeg;base64,' + img_str.decode("utf-8")
    #print(img_str)

    payload = {'image': img_str }

    headers = {}

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text.encode('utf8'))
    name = json.loads(response.text.encode('utf8'))['user']
    print("Detected user : {0}".format(name))
    return str(name).strip()


prev_face_center = None


# call face_detect function with a colorful frame
# if you need output face encoding, e.g. for name search in database, turn output_enc on,
# if you need output face crop for demo turn output_face on
# the output of this function is a list of num_face faces (default to 1)
# each element of this list is a tuple of (tuple of face_bounding_rect, move_direction, face_enc, face_crop)


class GameObject(object):
    def __init__(self, canvas, item):
        self.canvas = canvas
        self.item = item

    def get_position(self):
        return self.canvas.coords(self.item)

    def move(self, x, y):
        self.canvas.move(self.item, x, y)

    def delete(self):
        self.canvas.delete(self.item)


class Ball(GameObject):
    def __init__(self, canvas, x, y):
        self.radius = 15
        self.direction = [1, -1]
        self.speed = 20
        item = canvas.create_oval(x - self.radius, y - self.radius,
                                  x + self.radius, y + self.radius,
                                  fill='white')
        super(Ball, self).__init__(canvas, item)

    def update(self):
        coords = self.get_position()
        width = self.canvas.winfo_width()
        if coords[0] <= 0 or coords[2] >= width:
            self.direction[0] *= -1
        if coords[1] <= 0:
            self.direction[1] *= -1
        x = self.direction[0] * self.speed
        y = self.direction[1] * self.speed
        self.move(x, y)

    def collide(self, game_objects):
        coords = self.get_position()
        x = (coords[0] + coords[2]) * 0.5
        if len(game_objects) > 1:
            self.direction[1] *= -1
        elif len(game_objects) == 1:
            game_object = game_objects[0]
            coords = game_object.get_position()
            if x > coords[2]:
                self.direction[0] = 1
            elif x < coords[0]:
                self.direction[0] = -1
            else:
                self.direction[1] *= -1

        for game_object in game_objects:
            if isinstance(game_object, Brick):
                game_object.hit()


class Paddle(GameObject):
    def __init__(self, canvas, x, y):
        self.width = 300
        self.height = 20
        self.ball = None
        item = canvas.create_rectangle(x - self.width / 2,
                                       y - self.height / 2,
                                       x + self.width / 2,
                                       y + self.height / 2,
                                       fill='yellow')
        super(Paddle, self).__init__(canvas, item)

    def set_ball(self, ball):
        self.ball = ball

    def move(self, offset):
        coords = self.get_position()
        width = self.canvas.winfo_width()
        if coords[0] + offset >= 0 and coords[2] + offset <= width:
            super(Paddle, self).move(offset, 0)
            if self.ball is not None:
                self.ball.move(offset, 0)


class Brick(GameObject):
    COLORS = {1: '#e74c3c', 2: '#3498db', 3: '#222222'}

    def __init__(self, canvas, x, y, hits):
        self.width = 238.75
        self.height = 50
        self.hits = hits
        color = Brick.COLORS[hits]
        item = canvas.create_rectangle(x - self.width / 2,
                                       y - self.height / 2,
                                       x + self.width / 2,
                                       y + self.height / 2,
                                       fill=color, tags='brick')
        super(Brick, self).__init__(canvas, item)

    def hit(self):
        self.hits -= 1
        if self.hits == 0:
            self.delete()
        else:
            self.canvas.itemconfig(self.item,
                                   fill=Brick.COLORS[self.hits])


def restart():
    """Restarts the current program.
    Note: this function does not return. Any cleanup action (like
    saving data) must be done before calling this function."""
    python = sys.executable
    os.execl(python, python, *sys.argv)


class Game(tk.Frame):
    def __init__(self, master):
        super(Game, self).__init__(master)
        self.cam = cv2.VideoCapture(0)

        self.cnnFaceDetector = dlib.cnn_face_detection_model_v1("app/mmod_human_face_detector.dat")
        self.player_name = 'Unknown'

        self.lives = 0
        self.width = 1920
        self.height = 1080
        self.canvas = tk.Canvas(self, bg='#aaaaff',
                                width=self.width,
                                height=self.height, )
        self.canvas.pack()
        self.pack()
        self.text = None

        self.items = {}
        self.ball = None
        self.paddle = Paddle(self.canvas, self.width / 2, 1000)
        self.items[self.paddle.item] = self.paddle
        for x in range(5, self.width - 5, 239):
            self.add_brick(x + 119.375, 65, 2)
            self.add_brick(x + 119.375, 115, 1)
            self.add_brick(x + 119.375, 165, 1)

        self.hud = None

        self.canvas.focus_set()
        self.setup_game()
        # self.canvas.bind('<Left>',
        #                  lambda _: self.paddle.move(-30))
        # self.canvas.bind('<Right>',
        #                  lambda _: self.paddle.move(30))

    def face_detect(self, frame, ratio=1, num_face=1, output_enc=False, output_face=False):
        # input frame/image should be in RGB order
        assert (frame.ndim == 3), "Input video must be colorful with 3 channels!"
        detection_results = []
        face_move = 0
        # global prev_face_center
        _, width, _ = frame.shape
        frame_center_hor = width // 2
        bd_v = 30
        bd_h = 10
        scale = round(1.0 / ratio)
        if ratio < 1:
            small_frame = cv2.resize(frame, (0, 0), fx=ratio, fy=ratio)  # small_frame in RGB order
        else:
            small_frame = frame

        # run this on CPU
        # face_locations = face_recognition.face_locations(small_frame)
        # face_encodings = face_recognition.face_encodings(small_frame, face_locations)

        # run this on GPU, much faster
        face_locations = []
        face_encodings = []
        dets = self.cnnFaceDetector(small_frame)
        for d in dets:
            face_locations.append((max(d.rect.top(), 0), min(d.rect.right(), small_frame.shape[1]),
                                   min(d.rect.bottom(), small_frame.shape[0]), max(d.rect.left(), 0)))
        if face_locations:
            if output_enc:
                face_encodings = face_recognition.face_encodings(small_frame, face_locations)
            else:
                face_encodings = [None] * len(face_locations)

        for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
            # calculate the detected face region in original frame
            top *= scale
            right *= scale
            bottom *= scale
            left *= scale
            t = 0 if top < bd_v else top - bd_v
            l = 0 if left < bd_h else left - bd_h
            b = frame.shape[0] if bottom + bd_v > frame.shape[0] else bottom + bd_v
            r = frame.shape[1] if right + bd_h > frame.shape[1] else right + bd_h
            if output_face:
                cropped_face = frame[t:b, l:r, :]
            else:
                cropped_face = None

            face_center = ((l + r) // 2, (t + b) // 2)  # calculate the center of the detected face in (x, y) format
            rel_shift_hor = (face_center[0] - frame_center_hor) / width
            if rel_shift_hor >= 0.2:
                face_move = 2
            elif rel_shift_hor > 0.05:  # 5% of right shift, this threshold can be adjusted according to real scenarios
                face_move = 1
            if rel_shift_hor <= -0.2:
                face_move = -2
            elif rel_shift_hor < -0.05:
                face_move = -1
            # Since we use joystick-like input the following code and global var prev_face_center are commented out
            # if prev_face_center is not None:
            #     if prev_face_center[0] - face_center[0] >= width/10:
            #         face_move = -1
            #     elif prev_face_center[0] - face_center[0] <= -width/10:
            #         face_move = 1
            #     else:
            #         face_move = 0
            # prev_face_center = face_center
            detection_results.append(((t, r, b, l), face_move, face_encoding, cropped_face))
        # temporarily use this to limit the number of people recognized down to 2, need further improvement
        if len(detection_results) > num_face:
            detection_results = detection_results[0:num_face]

        return detection_results

    def detect_face_to_start(self):
        ret, frame = self.cam.read()
        ratio = 1
        assert (frame.ndim == 3), "Input video must be colorful with 3 channels!"
        _, width, _ = frame.shape
        frame_center_hor = width // 2
        bd_v = 30
        bd_h = 10
        scale = round(1.0 / ratio)
        if ratio < 1:
            small_frame = cv2.resize(frame, (0, 0), fx=ratio, fy=ratio)  # small_frame in RGB order
        else:
            small_frame = frame

        dets = self.cnnFaceDetector(small_frame)
        if len(dets):
            return True
        return False

    def restart_all(self):
        del self.cam
        restart()

    def reset(self):
        del self.text
        self.lives = 0
        self.width = 1920
        self.height = 1080
        self.canvas = tk.Canvas(self, bg='#aaaaff',
                                width=self.width,
                                height=self.height, )
        self.canvas.pack()
        self.pack()

        self.items = {}
        self.ball = None
        self.paddle = Paddle(self.canvas, self.width / 2, 1000)
        self.items[self.paddle.item] = self.paddle
        # for x in range(5, self.width - 5, 239):
        #    self.add_brick(x + 119.375, 65, 2)
        #    self.add_brick(x + 119.375, 115, 1)
        #    self.add_brick(x + 119.375, 165, 1)

        self.hud = None

        self.canvas.focus_set()
        self.setup_game()

    def random_moves(self):
        ri = randint(1, 100)
        move = ''
        if ri % 2 == 0:
            self.paddle.move(30)
            move = 'right'
        else:
            self.paddle.move(-30)
            move = 'left'
        return ri, move

    def move_by_face_position(self):
        ret, frame = self.cam.read()
        det_results = self.face_detect(frame)
        # print(det_results)
        if len(det_results):
            if len(det_results[0]):
                det_results_pos = det_results[0][1]
                if int(det_results_pos) > 0:
                    print("Face Pos :{0}\tMoving LEFT ".format(det_results_pos))
                    self.paddle.move(-30)
                elif int(det_results_pos) < 0:
                    print("Face Pos :{0}\tMoving RIGHT ".format(det_results_pos))
                    self.paddle.move(30)
                else:
                    print("Face Pos :{0}\t IDLE".format(det_results_pos))
                    # self.paddle.move(0)
            else:
                pass
                # ri, move = self.random_moves()
                # print("Making a random move :", ri, move)
        else:
            pass
            # ri, move = self.random_moves()
            # print("Making a random move :", ri, move)

    def setup_game(self):
        # reset hud
        self.canvas.itemconfig(self.hud, text='')
        # reset bricks
        for x in range(5, self.width - 5, 239):
            self.add_brick(x + 119.375, 65, 2)
            self.add_brick(x + 119.375, 115, 1)
            self.add_brick(x + 119.375, 165, 1)

        self.add_ball()
        self.canvas.delete(self.text)
        self.text = self.draw_text(self.width / 2, self.height / 2, 'Press Spacebar to begin Experience !')
        self.canvas.bind('<space>', lambda _: self.start_game())

    def add_ball(self):
        if self.ball is not None:
            self.ball.delete()
        paddle_coords = self.paddle.get_position()
        x = (paddle_coords[0] + paddle_coords[2]) * 0.5
        self.ball = Ball(self.canvas, x, 960)
        self.paddle.set_ball(self.ball)

    def add_brick(self, x, y, hits):
        brick = Brick(self.canvas, x, y, hits)
        self.items[brick.item] = brick

    def draw_text(self, x, y, text, size='40'):
        font = ('Forte', size)
        return self.canvas.create_text(x, y, text=text,
                                       font=font)

    def update_lives_text(self):
        ret, frame = self.cam.read()
        cv2.imwrite('test.jpg', frame)
        retval, buffer = cv2.imencode('.jpg', frame)
        jpg_as_text = base64.b64encode(buffer)
        self.player_name = find_who(jpg_as_text)

        with open('gamelog.csv', 'a') as f:
            f.write('{0},Started,{1}\n'.format(self.player_name, time.ctime()))
        text = 'Player: {0}'.format(self.player_name)
        if self.hud is None:
            self.hud = self.draw_text(self.width / 2, 20, text, 15)
        else:
            self.canvas.itemconfig(self.hud, text=text)

    def start_game(self):
        self.canvas.unbind('<space>')
        self.canvas.delete(self.text)
        self.update_lives_text()
        print("start game")
        self.paddle.ball = None
        self.game_loop()

    def game_loop(self):
        self.check_collisions()
        num_bricks = len(self.canvas.find_withtag('brick'))
        if num_bricks == 0:
            self.ball.speed = None
            self.after(5000, self.setup_game)
            self.text = self.draw_text(self.width / 2, self.height / 2,
                                       'You Win !!! Congratulations {0}'.format(self.player_name))
            with open('gamelog.csv', 'a') as f:
                f.write('{0},Won,{1}\n'.format(self.player_name, time.ctime()))
        elif self.ball.get_position()[3] >= self.height:
            self.ball.speed = None
            self.lives -= 1
            if self.lives < 0:
                self.after(5000, self.setup_game)
                self.text = self.draw_text(self.width / 2, self.height / 2, 'Game Over !')
                with open('gamelog.csv', 'a') as f:
                    f.write('{0},Lost,{1}\n'.format(self.player_name, time.ctime()))
            else:
                self.after(1000, self.setup_game)
        else:
            self.ball.update()
            start = time.time()
            self.move_by_face_position()
            # self.random_moves()
            print("Time spent :", time.time() - start)
            self.after(50, self.game_loop)

    def check_collisions(self):
        ball_coords = self.ball.get_position()
        items = self.canvas.find_overlapping(*ball_coords)
        objects = [self.items[x] for x in items if x in self.items]
        self.ball.collide(objects)


if __name__ == '__main__':
    root = tk.Tk()
    root.title('CS445 Final Project - Face Recognition Game')
    game = Game(root)
    game.mainloop()
