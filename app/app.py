# Author : Vivek Gnanavelu
# CS445 Final Project
# 04/20/2020

import base64
import io

from PIL import Image
from flask import Flask, request, render_template, session, jsonify, flash, redirect, url_for, send_file, \
    send_from_directory, Response, make_response
import numpy as np
import time

import cv2
import dlib, face_recognition

app = Flask(__name__)
app.secret_key = 'vkshd!@£ASD!@L£KM'

# this should be initialized globally
cnnFaceDetector = dlib.cnn_face_detection_model_v1("./mmod_human_face_detector.dat")

import psycopg2

db = ""
try:
    connection = psycopg2.connect(user = "postgres",
                                  password = "faceapp",
                                  host = "192.168.80.3",
                                  port = 5432,
                                  database = "postgres")

    connection.autocommit = True

    cursor = connection.cursor()
    # Print PostgreSQL Connection properties
    print ( connection.get_dsn_parameters(),"\n")

    # Print PostgreSQL version
    cursor.execute("SELECT version();")
    record = cursor.fetchone()
    print("You are connected to - ", record,"\n")
    db = cursor

except (Exception, psycopg2.Error) as error :
    print ("Error while connecting to PostgreSQL", error)


def store_in_db(name, face_encoding):
    app.logger.info("Storing into DB ... Name : {0}".format(str(name)))
    if name:
        if len(face_encoding) > 0:
            query = "INSERT INTO vectors (file, vec_low, vec_high) VALUES ('{}', CUBE(array[{}]), CUBE(array[{}]))".format(
                name,
                ','.join(str(s) for s in face_encoding[0:64]),
                ','.join(str(s) for s in face_encoding[64:128]),
            )
            db.execute(query)
    return


# retreives matches from db
def matches_from_db(detection_results):
    app.logger.info("Tring to find matches ...")
    # db = postgresql.open('pq://user:pass@localhost:5434/db')
    matches =[]
    for _, face_encoding, face in detection_results:
        # i commented matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding, tolerance=0.4)
        threshold = 0.6
        if len(face_encoding) > 0:
            query = "SELECT * FROM vectors WHERE sqrt(power(CUBE(array[{}]) <-> vec_low, 2) + " \
                    "power(CUBE(array[{}]) <-> vec_high, 2)) <= {} " \
                        .format(','.join(str(s) for s in face_encoding[0:64]),
                                ','.join(str(s) for s in face_encoding[64:128]), threshold, ) + \
                    "ORDER BY sqrt(power(CUBE(array[{}]) <-> vec_low, 2) + power(CUBE(array[{}]) <-> vec_high, 2)) ASC LIMIT 1".format(
                        ','.join(str(s) for s in face_encoding[0:64]),
                        ','.join(str(s) for s in face_encoding[64:128]),
                    )
            app.logger.info("\n\n{0}\n\n".format(query))
            matches = db.execute(query)
    return matches


# retreive all values in db if all = 1 else retreive values after the given count value(all must be zero)
def retreive_from_db(all, count):
    if all == 1:
        query = "SELECT * FROM vectors"
    elif all == 0 and count > 0:
        query = "SELECT * FROM vectors OFFSET" + count
    else:
        query = ""
    faces = db.execute(query)
    return faces

def convert_to_array(face):
    face = face.replace(" ", "")
    face = face[1:-1]
    face = face.split(',')
    face = np.asarray(face, dtype=np.float64)
    return face


def load_from_db():
    query = "SELECT * FROM vectors"
    db.execute(query)
    known_faces = db.fetchall()
    app.logger.info("DB Known Faces : {0}".format(len(known_faces)))
    if known_faces is not None:
        known_face_names = [face[1] for face in known_faces]
        known_face_encodings = []
        for face in known_faces:
            face_2 = np.asarray(convert_to_array(face[2]), dtype=np.float64)
            face_3 = np.asarray(convert_to_array(face[3]), dtype=np.float64)
            npa_face = np.concatenate((face_2, face_3), axis=0)
            known_face_encodings.append(npa_face)
        npa_known_face_encodings = np.asarray(known_face_encodings)
        return known_face_names, npa_known_face_encodings
    return None, None

def get_camera():
    camera = Camera()
    return camera


def gen(camera):
    while True:
        frame = camera.get_feed()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed/')
def video_feed():
    camera = get_camera()
    return Response(gen(camera),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


class Camera(object):
    CAPTURES_DIR = "static/captures/"

    def __init__(self):
        self.video = cv2.VideoCapture(0)

    def get_frame(self):
        success, frame = self.video.read()
        ...
        return frame

    def get_feed(self):
        frame = self.get_frame()
        if frame is not None:
            ret, jpeg = cv2.imencode('.jpg', frame)
            return jpeg.tobytes()

    def capture(self):
        frame = self.get_frame()
        timestamp = time.strftime("%d-%m-%Y-%Hh%Mm%Ss", time.localtime())
        filename = Camera.CAPTURES_DIR + timestamp + ".jpg"
        if not cv2.imwrite(filename, frame):
            raise RuntimeError("Unable to capture image " + timestamp)
        return timestamp

def detect_face(name, frame, ratio=1, output_enc=True, output_face=True):
    """ Detects face and crops """
    """ Output is a list of detection results each of which is a list of location, encoding and cropped face"""
    # input frame/image should be in RGB order
    assert (frame.ndim == 3), "Input video must be colorful!"
    detection_results = []
    bd_v = 30
    bd_h = 10
    scale = round(1.0 / ratio)
    if ratio < 1:
        small_frame = cv2.resize(frame, (0, 0), fx=ratio, fy=ratio)  # small_frame in RGB order
    else:
        small_frame = frame

    # find face locations
    # start_time = time.time()
    face_locations = []
    face_encodings = []
    dets = cnnFaceDetector(small_frame)

    for d in dets:
        print("DEBUG : Found detections !")
        face_locations.append((max(d.rect.top(), 0), min(d.rect.right(), small_frame.shape[1]),
                               min(d.rect.bottom(), small_frame.shape[0]), max(d.rect.left(), 0)))

    if face_locations:
        if output_enc:
            face_encodings = face_recognition.face_encodings(small_frame, face_locations)
        else:
            face_encodings = [None] * len(face_locations)

    for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
        # calculate the detected face region in original frame
        top *= scale
        right *= scale
        bottom *= scale
        left *= scale
        t = 0 if top < bd_v else top - bd_v
        l = 0 if left < bd_h else left - bd_h
        b = frame.shape[0] if bottom + bd_v > frame.shape[0] else bottom + bd_v
        r = frame.shape[1] if right + bd_h > frame.shape[1] else right + bd_h
        if output_face:
            cropped_face = frame[t:b, l:r, :]
        else:
            cropped_face = None
        detection_results.append(((t, r, b, l), face_encoding, cropped_face))

    return detection_results, face_encoding


def save_received_image(id, encoded_str):
    """ saves the received image in base64 encoded format"""
    try:
        # decods the base64 image string and saves the image file
        img = Image.open(io.BytesIO(base64.b64decode(encoded_str)))
        img.save('images/' + str(id) + '.jpg')
        return img
    except Exception as exp:
        app.logger.error("Error while processing the received image \n{0}".format(str(exp)))
    return 0


@app.route('/')
def home():
    return redirect(url_for('signup'))

@app.route('/recognize', methods=['POST'])
def recognize():
    try:
        if 'image' in request.form:
            image = request.form['image']
            decoded_data = base64.b64decode(image.split(',')[1])
            np_data = np.fromstring(decoded_data, np.uint8)
            img = cv2.imdecode(np_data, cv2.IMREAD_UNCHANGED)

            detections, face_encoding = detect_face('test', img, 1)
            all_names, all_encodings = load_from_db()

            if len(all_names):
                matches = face_recognition.compare_faces(all_encodings, face_encoding, tolerance=0.4)
                app.logger.info("No. of Matches (in DB): {0}".format(matches))
                if True in matches:
                    first_match_index = matches.index(True)
                    name = all_names[first_match_index]
                    app.logger.info("Detected already registered user : {0}".format(name))
                    return make_response(jsonify(user=name), 200)
                else:
                    return make_response(jsonify(user='Unknown'), 200)
            else:
                return make_response(jsonify(user='Unknown'), 200)
        else:
            return make_response(jsonify(error='image key missing in input form data'), 400)
    except Exception as e:
        app.logger.error("Error while processing request : \n{0}".format(str(e)))
        return make_response(jsonify(error='Error while processing. Please retry with correct input.'), 501)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        name = request.form['name']
        image = request.form['file']
        app.logger.info("Name : {0}".format(name))
        app.logger.info("Image : {0}".format(len(image)))

        decoded_data = base64.b64decode(image.split(',')[1])
        np_data = np.fromstring(decoded_data, np.uint8)
        img = cv2.imdecode(np_data, cv2.IMREAD_UNCHANGED)
        # print(img)
        app.logger.info("image : {0}\nlength : {1}".format(img, len(img)))
        detections, face_encoding = detect_face(name, img, 1)

        all_names, all_encodings = load_from_db()
        if len(all_names):
            matches = face_recognition.compare_faces(all_encodings, face_encoding, tolerance=0.4)
            app.logger.info("No. of Matches (in DB): {0}".format(matches))
            if True in matches:
                first_match_index = matches.index(True)
                name = all_names[first_match_index]
                app.logger.info("Detected already registered user : {0}".format(name))
                print("It exists in DB !")
                return make_response(jsonify(registered_user=name), 201)
                # return Response("{'status':'ALREADY EXISTS', 'registered_user': '{0}'}".format(name), status=201, mimetype='application/json')
            else:
                store_in_db(name, face_encoding)
                return Response("{'status':'SUCCESS'}", status=200, mimetype='application/json')
        else:
            store_in_db(name, face_encoding)
            return Response("{'status':'SUCCESS'}", status=200, mimetype='application/json')
        return jsonify(request.form['name'], request.form['file'])
    return render_template('signup.html')


class MyVideoCapture:
    def __init__(self, video_source=0):
        # Open the video source
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                # Return a boolean success flag and the current frame converted to RGB
                return ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        else:
            return False, None

    # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True)
