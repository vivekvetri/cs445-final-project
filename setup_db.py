# Author : Vivek Gnanavelu
# CS445 Final Project
# 04/16/2020

import psycopg2

def setup_db(db):
    """ For setting up vectors table, index and cube extension for storing face encodings """
    db.execute("create extension if not exists cube;")
    db.execute("drop table if exists vectors")
    db.execute("create table vectors (id serial, file varchar, vec_low cube, vec_high cube);")
    db.execute("create index vectors_vec_idx on vectors (vec_low, vec_high);")
    db.execute("commit")
    db.close()
    print('done !')

def main():
    try:
        # Connection string
        connection = psycopg2.connect(user = "postgres",
                                      password = "faceapp",
                                      host = "localhost",
                                      port = "5432",
                                      database = "postgres")

        cursor = connection.cursor()
        # Print PostgreSQL Connection properties
        print ( connection.get_dsn_parameters(),"\n")
        # Print PostgreSQL version
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print("You are connected to - ", record,"\n")
        print("Setting up necessary DB and index ! ")

        # Setting up vectors table and index to store the face encodings
        setup_db(cursor)

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)

if __name__ == '__main__':
    main()
