# Overview

The entire project is divided into two major components, 

* Registration
* Game with Face Recognition

# High Level Architecture

![architecture](images/architecture.png)

# I. Face Registration App

To avoid environment setup and DB setup, i have created everything as docker containers and wrote a docker compose file to deploy them as stack in a proper way. Please find the following setup guide and usage instructions. Only requirements are docker engine and internet connectivity to setup this face registration app.

## Container Details

* faceapp - based on python 3.7 docker image with all dependencies needed to host the web registration application like flask, opencv, postgres client, videojs, webcamjs, etc. API endpoint /recognize will be used later by the game app
* faceapp-postgres - simple single node postgresDB to store the registered faces. Will be accessed by faceapp (signup and recognize endpoints)

## Setup

* Install docker engine and docker-compose utility script and run the following command.

```
docker-compose build
docker-compose up -d
```
* Please run the following python script to setup the postgres db with relevant DB. You will see messages like 'You are connected to DB' and 'Setting up necessary DB and index !' if it successful.

```
python3 setup_db.py
```

## App Usage [Basic]

[http://localhost:5000/](http://localhost:5000/)

* Self registration app.
* Click a snap of your face, enter your name and click submit
* It will notify - 'Registered successfully' if everything went well, if not 'Unable to detect face'. If already registered, it will say 'Already registered'
* Once successfully registered, the face and name will be stored in PostgresDB for later recognition by Game.

## API Usage [Advanced]

I have also created an endpoint /recognize to use in my game app. It could be used for testing as well. Here is an example curl command to send an image (in base64encoded format) to /recognize endpoint and get detected user as response.

### Sample Request using Postman tool:

![postman](images/api_postman.png)

### Sample Request using Curl:

```
curl --location --request POST 'localhost:5000/recognize' \
--form 'image=data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAgQDAgICAgUEBAMEBgUGBgYFBgYGBwkIBgcJBwYGCAsICQoKCgoKBggLDAsKDAkKCgr/2wBDAQICAgICAgUDAwUKBwYHCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgr/wAARCALTAnEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/…'
```

### Sample Response :

#### Unidentified user

```
{
	"user": "Unknown"
}
```

**[OR]**

#### Identified user

```
{
	"user": "vivek"
}
```

**Note:** Same /recognize endpoint is used for recognition by the game.

# II. Game with Face Recognition

This is a classic brick breakout game. Paddle in this move will be moved left or right based on user face movements across the camera.

## Setup

* Make sure camera is not used by any other application
* Install all relevant packages in 'game_requirements.txt' using the following command,

```
pip3 install -r game_requirements.txt
```

* Run the game using the following command,

```
python3 game.py
```

* 'Press Space bar to begin experience' message will come. Please proceed by looking at the camera and pressing space bar. 


![game](images/game.png)

* Game will recognize the player using the camera and let them play by moving left or right inside the game.

![game](images/game_player.png)
